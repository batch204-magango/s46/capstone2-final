const mongoose = require("mongoose");




//option 2
//updated 10-18-22

const userSchema = new mongoose.Schema({
	email : {
		type : String,
	},
	password : {
		type : String,
		required : [true, "Password is required"]
	},
	isAdmin : {
		type : Boolean,
		default : false
	},
	orders : [
		{
			products : [
				{
					productId : {
						type : String
					},
					productName : {
						type: String
					},
					price : {
						type: Number
					},
					quantity : {
						type: Number
					}	
				}
			],	
			totalAmount : {
				type : Number
			},
			purchasedOn : {
				type : Date,
				default : new Date()
			}
		}
	]
	
})

module.exports = mongoose.model("User", userSchema);
