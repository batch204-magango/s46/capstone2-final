const Product = require("../models/Product");
const User = require("../models/User");
const Order = require("../models/Order");
//Create new order
module.exports.createNewOrder = (reqBody, data) => {
	return User.findById(data.userId).then(result => {
		if (data.isAdmin === true) { //if admin = true
			return false
		} else { //if admin = false
			let newOrder = new Order ({
				userId: reqBody.userId,
				products:reqBody.products,
				totalAmount: reqBody.totalAmount
			});

			return newOrder.save().then((order, error) => {
				if (error) { // order failed
					return `Order Failed`
				} else {
					return `Order Success`
				}
			});
		}
	});
}