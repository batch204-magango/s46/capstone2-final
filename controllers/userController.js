const User = require("../models/User");
const Product = require("../models/Product");

const bcrypt = require("bcrypt");
const auth = require("../auth");

//Check if email exists
module.exports.checkEmailExists = (reqBody) => {

	return User.find({email: reqBody.email}).then(result => {

		if(result.length > 0) {
			return `Email exist in the database`
		} else {
			return `User not found`
		}

	})
}



//Controller for User Registration
//new registration
module.exports.registerUser = (reqBody) => {

	return User.findOne({email: reqBody.email}).then(result => {

		if(result === null){

			if(reqBody.password.length >= 8){
				let newUser = new User({
					email: reqBody.email,
					password: bcrypt.hashSync(reqBody.password, 10)
				});

				return newUser.save().then((user, error) => {

					if(error) {
						return false
					} else {
						return 'Registered Successfully'
					}
				})
			}
			else{
				return `Password is too weak`
			}		
		}
		else{
			return `User is already registered`
		}
	})
}






//User Authentication
module.exports.loginUser = (reqBody)=> {
	return User.findOne({email: reqBody.email}).then(result => {
		if(result == null) {
			return false
		} else {

			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password);
			
			if(isPasswordCorrect) {
				console.log(result)
				return {
					access:auth.createAccessToken(result)}
			} else {
				return false
			}
		}
	})
}


module.exports.getUser = (reqParams) => {


	return User.findById(reqParams.userId).then(result => {
		
		result.password = "";
		return result
	
	})

}







//retrieve profile

module.exports.getProfile = (reqBody) => {

	return User.findById(reqBody.id).then(result => {
		result.password = ""
		return result
	}).catch(error => "id not found")
}

