const Product = require("../models/Product");
const User = require("../models/User");
// const productController = require("../controllers/productController");


//Create a New Product
//addproduct
module.exports.createProduct = (reqBody) => {

	let newProduct = new Product({

		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price
	});


	return newProduct.save().then( (product, error) => {

		if(error) {

			return "Adding Product Failed"

		} else {

			return 'Product Created'
		}

	});
};




//Retrieve all product
module.exports.getAllProduct =()=> {
	return Product.find({}).then(result => {
		return result;
	})
}


//controller for retrieving all active product
module.exports.getAllActive = () => {
	return Product.find({isActive:true}).then (result => {
		return result;
	})
}

//Retrieving a specific product
module.exports.getProduct = (reqParams) => {
	return Product.findById(reqParams.productId).then(result => {
		return result
	})
}




//Updating Product
module.exports.updateProduct = (reqParams, reqBody, data) => {

    return User.findById(data.id).then(result => {
        console.log(result)

        if(result.isAdmin === true) {

            // Specify the fields/properties of the document to be updated
            let updatedProduct = {
                name: reqBody.name,
                description: reqBody.description,
                price: reqBody.price
            };

           
            return Product.findByIdAndUpdate(reqParams.productId, updatedProduct).then((product, error) => {

                // product not updated
                if(error) {

                    return 'Product not updated'

                // product updated successfully    
                } else {

                    return 'Product updated successfuly'
                }
            })

        } 
        else {

            return 'User is not Admin'
        }



    })


}

//archive new 
module.exports.archiveProduct = (data, reqBody) => {

	return Product.findById(data.productId).then(result => {

		if (data.payload === true) {

			let updateActiveField = {
				isActive: false
			}

			return Product.findByIdAndUpdate(result._id, updateActiveField).then((product, err) => {

				if(err) {
					
					return false
					
				}  
				else {

					return `Product Archived`
				}
			})		 
		} 
		else {
			return `User is not  admin`
		} 
	})
}
