const express = require("express");
const router = express.Router();
const productController = require("../controllers/productController");
const auth = require('../auth.js');


//Route for creating a product
router.post("/", auth.verify, (req, res) => {

	const isAdmin = auth.decode(req.headers.authorization).isAdmin

	if(isAdmin) {
		productController.createProduct(req.body).then(resultFromController => res.send(resultFromController));
	} else {
		res.send(`User is not an admin`);
	}
});





//Route for retrieving all the product
router.get("/all", (req, res)=> {
	productController.getAllProduct().then(resultFromController => res.send(resultFromController));
});







//Route for retrieving all the active products
router.get("/active", (req, res)=> {
	productController.getAllActive().then(resultFromController => res.send(resultFromController));
});



//Routes for retrieving specific product

router.get("/:productId", (req, res)=>{
	console.log(req.params)

	productController.getProduct(req.params).then(resultFromController => res.send(resultFromController))
})


router.put("/:productId", auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization)

	productController.updateProduct(req.params, req.body, userData).then(resultFromController => res.send(resultFromController));
});






//Archiving product
router.get('/:productId/archive', auth.verify, (req, res) => {

	const data = {
		productId : req.params.productId,
		payload : auth.decode(req.headers.authorization).isAdmin
	}

	productController.archiveProduct(data).then(resultFromController => res.send(resultFromController))
});







module.exports = router;